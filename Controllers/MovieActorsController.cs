﻿using Api.MovieActors;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using MyImdb.Services;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/movie-actors")]
	public class MovieActorsController : Controller {
		private readonly MovieActorService movieActorService;
		private readonly ModelConverter mc;
		private readonly MovieActorRepository movieActorRepository;

		public MovieActorsController(
			MovieActorService movieActorService,
			ModelConverter mc,
			MovieActorRepository movieActorRepository
		) {
			this.mc = mc;
			this.movieActorRepository = movieActorRepository;
			this.movieActorService = movieActorService;
		}

		[HttpPost]
		public async Task<MovieActorModel> Create(MovieActorData request) {
			var movieActor = await movieActorService.CreateAsync(request);
			return mc.ToModel(movieActor);
		}

		[HttpGet("{id}")]
		public async Task<MovieActorModel> Get(Guid id) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);
			return mc.ToModel(movieActor!);
		}

		[HttpPut("{id}")]
		public async Task<MovieActorModel> Update(Guid id, MovieActorData request) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);
			await movieActorService.UpdateAsync(movieActor!, request);
			return mc.ToModel(movieActor!);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movie = await movieActorRepository.SelectByIdAsync(id);
			await movieActorService.DeleteAsync(movie!);
		}
	}
}
