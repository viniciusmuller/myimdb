﻿using Api.Genres;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/genres")]
	public class GenresController : Controller {
		private readonly ModelConverter mc;
		private readonly GenreService genreService;
		private readonly GenreRepository genreRepository;

		public GenresController(ModelConverter mc, GenreService genreService, GenreRepository genreRepository) {
			this.mc = mc;
			this.genreService = genreService;
			this.genreRepository = genreRepository;
		}

		[HttpGet("{id}")]
		public async Task<GenreModel> Get(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);
			return mc.ToModel(genre!);
		}

		[HttpGet]
		public async Task<List<GenreModel>> List(int n = 20) {
			var genres = await genreRepository.SelectTopNAsync(n);
			return genres.ConvertAll(g => mc.ToModel(g));
		}

		[HttpPost]
		public async Task<GenreModel> Create(GenreData request) {
			var genre = await genreService.CreateAsync(request);
			return mc.ToModel(genre);
		}

		[HttpPut("{id}")]
		public async Task<GenreModel> Update(Guid id, GenreData request) {
			var genre = await genreRepository.SelectByIdAsync(id);
			await genreService.UpdateAsync(genre!, request);
			return mc.ToModel(genre!);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);
			await genreService.DeleteAsync(genre!);
		}
	}
}
