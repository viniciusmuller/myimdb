﻿using Api.Movies;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;

namespace MyImdb.Controllers
{
    [ApiController]
    [Route("api/movies")]
    public class MoviesController : Controller
    {
        private readonly MovieService movieService;
        private readonly MovieRepository movieRepository;
        private readonly ModelConverter mc;

        public MoviesController(MovieService movieService, MovieRepository movieRepository, ModelConverter mc)
        {
            this.movieService = movieService;
            this.movieRepository = movieRepository;
            this.mc = mc;
        }

        // GET: Movie
        [HttpGet]
        public async Task<List<MovieModel>> List(int n = 20)
        {
            var movies = await movieRepository.SelectTopNAsync(n);
            return movies.ConvertAll(m => mc.ToModel(m));
        }

        [HttpPost]
        public async Task<MovieModel> Create(MovieData request)
        {
            var movie = await movieService.CreateAsync(request);
            return mc.ToModel(movie);
        }

        [HttpGet("{id}")]
        public async Task<MovieModel> Get(Guid id)
        {
            var movie = await movieRepository.SelectByIdAsync(id);
            return mc.ToModel(movie!);
        }

        [HttpPut("{id}")]
        public async Task<MovieModel> Update(Guid id, MovieData request)
        {
            var movie = await movieRepository.SelectByIdAsync(id);
            await movieService.UpdateAsync(movie!, request);
            return mc.ToModel(movie!);
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            var movie = await movieRepository.SelectByIdAsync(id);
            await movieService.DeleteAsync(movie!);
        }
    }
}
