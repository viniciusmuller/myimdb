﻿using Api.Actors;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/actors")]
	public class ActorsController : Controller {
		private readonly ModelConverter mc;
		private readonly ActorService actorService;
		private readonly ActorRepository actorRepository;

		public ActorsController(ModelConverter mc, ActorService actorService, ActorRepository actorRepository) {
			this.mc = mc;
			this.actorService = actorService;
			this.actorRepository = actorRepository;
		}

		[HttpGet("{id}")]
		public async Task<ActorModel> Get(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);
			return mc.ToModel(actor!);
		}

		[HttpGet]
		public async Task<List<ActorModel>> List(int n = 20) {
			var actors = await actorRepository.SelectTopNAsync(n);
			return actors.ConvertAll(g => mc.ToModel(g));
		}

		[HttpPost]
		public async Task<ActorModel> Create(ActorData request) {
			var actor = await actorRepository.CreateAsync(request);
			return mc.ToModel(actor);
		}

		[HttpPut("{id}")]
		public async Task<ActorModel> Update(Guid id, ActorData request) {
			var actor = await actorRepository.SelectByIdAsync(id);
			await actorService.UpdateAsync(actor!, request);
			return mc.ToModel(actor!);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);
			await actorService.DeleteAsync(actor!);
		}
	}
}
