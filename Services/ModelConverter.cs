﻿using Api.Actors;
using Api.Genres;
using Api.MovieActors;
using Api.Movies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Entities;

namespace MyImdb.Services
{
    public class ModelConverter
    {
        public ModelConverter() { }

        public GenreModel ToModel(Genre genre)
        {
            return new GenreModel()
            {
                Id = genre.Id,
                Name = genre.Name
            };
        }

        public MovieModel ToModel(Movie movie)
        {
           return new MovieModel()
            {
                Id = movie.Id,
                Title = movie.Title,
                Rank = movie.Rank,
                Year = movie.Year,
                Storyline = movie.Storyline,
                Genre = ToModel(movie.Genre),
                Actors = movie.MovieActors?.ConvertAll(x => ToModel(x)) ?? new()
            };
        }

        public MovieActorModel ToModel(MovieActor movieActor)
        {
            return new MovieActorModel()
            {
                Id = movieActor.Id,
                MovieId = movieActor.MovieId,
                ActorId = movieActor.ActorId,
                Character = movieActor.Character
            };
        }

        public ActorModel ToModel(Actor actor)
        {
            return new ActorModel()
            {
                Id = actor.Id,
                Birthplace = actor.Birthplace,
                Name = actor.Name,
                Movies = actor.MovieActors?.ConvertAll(x => x.MovieId) ?? new(),
            };
        }
    }
}
