﻿using Api;
using Api.Genres;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business {
	public class GenreService {
		private readonly GenreRepository genreRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly MovieService movieService;
		private readonly AppDbContext dbContext;

		public GenreService(
			GenreRepository genreRepository,
			ExceptionBuilder exceptionBuilder,
			MovieService movieService,
			AppDbContext dbContext
			) {
			this.genreRepository = genreRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.movieService = movieService;
			this.dbContext = dbContext;
		}

		public async Task<Genre> CreateAsync(GenreData req) {
			var repeated = await genreRepository.CheckIfGenreNameAlreadyExists(req, null);
			if (repeated) {
				throw exceptionBuilder.Api(Enums.ErrorCodes.GenreNameAlreadyExists, new { req.Name });
			}
			var genre = await genreRepository.CreateAsync(req.Name);
			await dbContext.SaveChangesAsync();
			return genre;
		}

		public async Task UpdateAsync(Genre genre, GenreData req) {
			var duplicated = await genreRepository.CheckIfGenreNameAlreadyExists(null, genre);
			if (duplicated) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.GenreNameAlreadyExists, new { genre!.Name });
			}

			genreRepository.Update(genre!, req);
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Genre genre) {
			var movies = await movieService.SelectByGenreIdAsync(genre.Id);
			genreRepository.Delete(genre);
			await movieService.RemoveMoviesAsync(movies);
			await dbContext.SaveChangesAsync();
		}
	}
}
