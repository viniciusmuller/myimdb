﻿using Api;
using Api.MovieActors;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business {
	public class MovieActorService {
		private readonly MovieActorRepository movieActorRepository;
		private readonly AppDbContext dbContext;
		private readonly MovieRepository movieRepository;
		private readonly ActorRepository actorRepository;

		public MovieActorService(
			AppDbContext dbContext,
			MovieActorRepository movieActorRepository,
			ActorRepository actorRepository,
			MovieRepository movieRepository
			) {
			this.movieActorRepository = movieActorRepository;
			this.dbContext = dbContext;
			this.actorRepository = actorRepository;
			this.movieRepository = movieRepository;
		}

		public async Task<MovieActor> CreateAsync(MovieActorData req) {
			_ = await actorRepository.SelectByIdAsync(req.ActorId);
			_ = await movieRepository.SelectByIdAsync(req.MovieId);

			var movieActor = await movieActorRepository.CreateAsync(req);
			await dbContext.SaveChangesAsync();
			return movieActor;
		}

		public async Task UpdateAsync(MovieActor movie, MovieActorData req) {
			_ = await actorRepository.SelectByIdAsync(req.ActorId);
			_ = await movieRepository.SelectByIdAsync(req.MovieId);

			movieActorRepository.Update(movie, req);
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(MovieActor movie) {
			movieActorRepository.Delete(movie);
			await dbContext.SaveChangesAsync();
		}
	}
}
