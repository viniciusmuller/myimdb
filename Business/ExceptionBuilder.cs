﻿using Api;
using System.Runtime.Serialization;
using static Api.Enums;

namespace MyImdb.Business
{
    public class ExceptionBuilder
    {
        public ApiException Api(ErrorCodes code, object details = null)
        {
            // - Refactored
            var message = code switch
            {
                ErrorCodes.Unknown => "An Unknown error has occurred",
                ErrorCodes.MovieTitleAlreadyExists => "A movie with the provided title already exists",
                ErrorCodes.GenreNotFound => "The provided genre does not exist",
                ErrorCodes.GenreNameAlreadyExists => "A genre with the provided name already exists",
                ErrorCodes.ActorNotFound => "The actor does not exist",
                ErrorCodes.MovieActorNotFound => "The MovieActor does not exist",
                ErrorCodes.MovieNotFound => "The movie does not exist",
                _ => string.Empty
            };

            return new ApiException(new ErrorModel()
            {
                Code = code,
                Message = message,
                Details = GetDetailsDictionary(details)
            });
        }
        private static Dictionary<string, string> GetDetailsDictionary(object details)
        {
            if (details == null)
            {
                return null;
            }
            var dic = new Dictionary<string, string>();
            foreach (var descriptor in details.GetType().GetProperties())
            {
                dic[descriptor.Name] = descriptor.GetValue(details, null)?.ToString() ?? "null";
            }
            return dic;
        }
    }
}
