﻿using Api.Actors;
using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class ActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public ActorRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<Actor> CreateAsync(ActorData req) {
			var actor = new Actor() {
				Id = Guid.NewGuid(),
				Name = req.Name,
				Birthplace = req.Birthplace
			};
			await dbContext.AddAsync(actor);
			return actor;
		}

		public async Task<Actor?> SelectByIdAsync(Guid id, bool raise = true) {
			var actor = await dbContext.Actors.FirstOrDefaultAsync(m => m.Id == id);
			if (actor == null && raise) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.ActorNotFound, new { id });
			}
			return actor;
		}

		public async Task<List<Actor>> SelectTopNAsync(int n = 20) {
			return await dbContext.Actors.Include(a => a.MovieActors)
				.OrderBy(m => m.Name).AsQueryable().Take(n).ToListAsync();
		}

		public void Update(Actor actor, ActorData req) {
			actor.Birthplace = req.Birthplace;
			actor.Name = req.Name;
		}

		public void Delete(Actor actor) {
			dbContext.Remove(actor);
		}
	}
}
