﻿using Api.MovieActors;
using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class MovieActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieActorRepository(
			AppDbContext dbContext,
			ExceptionBuilder exceptionBuilder
			) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<MovieActor> CreateAsync(MovieActorData req) {
			var movieActor = new MovieActor() {
				Id = new Guid(),
				MovieId = req.MovieId,
				ActorId = req.ActorId,
				Character = req.Character
			};
			await dbContext.AddAsync(movieActor);
			return movieActor;
		}

		public async Task<MovieActor?> SelectByIdAsync(Guid id, bool raise = true) {
			var movieActor = await dbContext.MovieActors.FirstOrDefaultAsync(g => g.Id == id);
			if (movieActor == null && raise) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieActorNotFound, new { id });
			}
			return movieActor;
		}

		public void Delete(MovieActor movieActor) {
			dbContext.Remove(movieActor);
		}

		public void Update(MovieActor movieActor, MovieActorData req) {
			movieActor.ActorId = req.ActorId;
			movieActor.MovieId = req.MovieId;
			movieActor.Character = req.Character;
		}
	}
}
