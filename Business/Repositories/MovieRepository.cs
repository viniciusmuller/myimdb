﻿using Api.Movies;
using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories
{
    public class MovieRepository
    {
        private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
            this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

        public async Task<List<Movie>> SelectTopNAsync(int n = 20)
        {
            return await dbContext.Movies.Include(m => m.Genre).Include(m => m.MovieActors)
                .OrderBy(m => m.Title).AsQueryable().Take(n).ToListAsync();
        }

        public async Task<Movie?> SelectByIdAsync(Guid id, bool raise = true)
        {
			var movie = await dbContext.Movies
				.Include(m => m.Genre).Include(m => m.MovieActors)
				.FirstAsync(m => m.Id == id);

			if (movie == null && raise) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieNotFound, new { id });
			}

			return movie;
        }

        public async Task<Movie?> SelectByTitleAsync(string title)
        {
            return await dbContext.Movies.FirstOrDefaultAsync(m => m.Title == title);
        }

        public async Task<List<Movie>> SelectByGenreIdAsync(Guid id)
        {
            return await dbContext.Movies.Where(m => m.GenreId == id).AsQueryable().ToListAsync();
        }

        public async Task<Movie> CreateAsync(MovieData req)
        {
            var movieId = Guid.NewGuid();
            var movie = new Movie()
            {
                Id = movieId,
                Rank = req.Rank,
                Title = req.Title,
                Year = req.Year,
                Storyline = req.Storyline,
                GenreId = req.GenreId,
                CreationDate = DateTimeOffset.Now,
            };
            await dbContext.AddAsync(movie);
            return movie;
        }
    }
}
