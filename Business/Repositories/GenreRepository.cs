﻿using Api.Genres;
using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class GenreRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public GenreRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<Genre?> SelectByIdAsync(Guid id, bool raise = true) {
			var genre = await dbContext.Genres.FirstOrDefaultAsync(g => g.Id == id);
			if (genre == null && raise) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.GenreNotFound, new { id });
			}
			return genre;

		}

		public async Task<List<Genre>> SelectTopNAsync(int n = 20) {
			return await dbContext.Genres.OrderBy(m => m.Name).AsQueryable().Take(n).ToListAsync();
		}

		public async Task<Genre> CreateAsync(string name) {
			var genre = new Genre() {
				Id = Guid.NewGuid(),
				Name = name,
			};
			await dbContext.AddAsync(genre);
			return genre;
		}

		public void Update(Genre genre, GenreData req) {
			genre.Name = req.Name;
		}

		public async Task<bool> CheckIfGenreNameAlreadyExists(GenreData? req, Genre? genre) {
			if (req == null && genre == null) {
				throw new ArgumentException("Either req or genre can be null");
			}

			if (genre != null) {
				// We are updating a genre, ignore our current gene
				return await dbContext.Genres.AnyAsync(g => g.Name == genre.Name && g.Id != genre.Id);
			} else {
				// We are adding a new genre, just check if the gene already exists
				return await dbContext.Genres.AnyAsync(g => g.Name == req.Name);
			}
		}

		public void Delete(Genre genre) {
			dbContext.Remove(genre);
		}
	}
}
