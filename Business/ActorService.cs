﻿using Api;
using Api.Actors;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business {
	public class ActorService {
		private readonly ActorRepository actorRepository;
		private readonly AppDbContext dbContext;

		public ActorService(ActorRepository actorRepository, AppDbContext dbContext) {
			this.actorRepository = actorRepository;
			this.dbContext = dbContext;
		}

		public async Task<Actor> CreateAsync(ActorData req) {
			var movie = await actorRepository.CreateAsync(req);
			await dbContext.SaveChangesAsync();
			return movie;
		}

		public async Task DeleteAsync(Actor actor) {
			actorRepository.Delete(actor);
			await dbContext.SaveChangesAsync();
		}

		public async Task<Actor> UpdateAsync(Actor actor, ActorData req) {
			actorRepository.Update(actor, req);
			await dbContext.SaveChangesAsync();
			return actor;
		}
	}
}
