﻿using Api.MovieActors;
using Api.Movies;
using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business {
	public class MovieService {
		private readonly MovieRepository movieRepository;
		private readonly GenreRepository genreRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;

		public MovieService(
			MovieRepository movieRepository,
			GenreRepository genreRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext
		) {
			this.movieRepository = movieRepository;
			this.genreRepository = genreRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
		}

		public async Task<Movie> CreateAsync(MovieData req) {
			var existingMovie = await movieRepository.SelectByTitleAsync(req.Title);
			if (existingMovie != null) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieTitleAlreadyExists, new { req.Title });
			}

			var existingGenre = await genreRepository.SelectByIdAsync(req.GenreId) != null;
			if (!existingGenre) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.GenreNotFound, new { req.GenreId });
			}

			var movie = await movieRepository.CreateAsync(req);
			await dbContext.SaveChangesAsync();
			return movie;
		}

		public async Task UpdateAsync(Movie movie, MovieData req) {
			if (await dbContext.Movies.AnyAsync(m => m.Title == req.Title && m.Id != movie.Id)) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieTitleAlreadyExists, new { req.Title });
			}

			movie.Title = req.Title;
			movie.Rank = req.Rank;
			movie.Year = req.Year;
			movie.Title = req.Title;
			movie.Storyline = req.Storyline;
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Movie movie) {
			dbContext.Remove(movie);
			await dbContext.SaveChangesAsync();
		}

		public async Task<List<Movie>> SelectByGenreIdAsync(Guid genreId) {
			return await movieRepository.SelectByGenreIdAsync(genreId);
		}

		public async Task RemoveMoviesAsync(List<Movie> movies) {
			dbContext.RemoveRange(movies);
			await dbContext.SaveChangesAsync();
		}
	}
}
