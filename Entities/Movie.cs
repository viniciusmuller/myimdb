﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyImdb.Entities
{
    public class Movie
    {
        public Guid Id { get; set; }
        public int Rank { get; set; }
        [MaxLength(100)]
        [Required]
        public string Title { get; set; }
        public int Year { get; set; }
        [MaxLength(200)]
        public string Storyline { get; set; }
        public DateTime CreationDateUtc { get; set; }
        public Guid GenreId { get; set; }
        public Genre Genre { get; set; }
        public List<MovieActor> MovieActors { get; set; }

        [NotMapped]
        public DateTimeOffset CreationDate
        {
            get
            {
                return new DateTimeOffset(CreationDateUtc, TimeSpan.Zero);
            }
            set
            {
                CreationDateUtc = value.UtcDateTime;
            }
        }

        #region RETRIEVE
        public static List<Movie> SelectAll()
        {
            var movies = new List<Movie>() {
                    new Movie() {
                        Id = new Guid(), Rank = 9, Title = "The Shawshank Redemption", Year =
                        1994, Storyline = "Two imprisoned men bond over a number of years..."
                    },
                    new Movie() {
                        Id = new Guid(), Rank = 9, Title = "The Godfather", Year = 1972,
                        Storyline = "The aging patriarch of an organized crime dynasty..."
                    },
                    new Movie() {
                        Id = new Guid(), Rank = 9, Title = "The Godfather - Part II", Year = 1974,
                        Storyline = "The early life and career of Vito Corleone in 1920s..."
                    },
                    new Movie() {
                        Id = new Guid(), Rank = 9, Title = "The Dark Knight", Year = 2008,
                        Storyline = "When the menace known as the Joker wreaks havoc..."
                    }
                    };
            return movies;
        }
        #endregion
    }
}
