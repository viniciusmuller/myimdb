using Microsoft.EntityFrameworkCore;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Configuration;
using MyImdb.Entities;
using MyImdb.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews(o => {
    o.Filters.Add<HandleExceptionFilter>();
    o.Filters.Add<ValidateModelStateAttribute>();
}).AddNewtonsoftJson();

builder.Host.ConfigureLogging((hostingContext, logging) =>
{
    logging.AddSerilog(new LoggerConfiguration().ReadFrom
        .Configuration(hostingContext.Configuration, "Serilog")
        .CreateLogger()
    );
});

// Db 
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

// Business
builder.Services.AddScoped<MovieRepository>();
builder.Services.AddScoped<MovieService>();
builder.Services.AddScoped<GenreRepository>();
builder.Services.AddScoped<GenreService>();
builder.Services.AddScoped<ActorRepository>();
builder.Services.AddScoped<ActorService>();
builder.Services.AddScoped<MovieActorRepository>();
builder.Services.AddScoped<MovieActorService>();
builder.Services.AddScoped<ExceptionBuilder>();
builder.Services.AddScoped<ModelConverter>();

var app = builder.Build();

updateDatabase(app);

// Add services to the container.
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}"
    );
    endpoints.MapControllers();
});

app.Run();

void updateDatabase(IHost host)
{
    using (var scope = host.Services.CreateScope())
    {
        var services = scope.ServiceProvider;
        try
        {
            var context =
            services.GetRequiredService<AppDbContext>();
            context.Database.Migrate();
        }
        catch (Exception ex)
        {
            var logger =
            services.GetRequiredService<ILogger<Program>>();
            logger.LogError(ex, "An error occurred migrating the DB.");
        }
    }
}
